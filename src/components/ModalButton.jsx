import React from "react";
import PropTypes from "prop-types";

export const ModalButton = ({ text, backgroundColor, onClick }) => {
  return (
    <button className="btn" style={{ backgroundColor }} onClick={onClick}>
      {text}
    </button>
  );
};

ModalButton.propTypes = {
  text: PropTypes.string,
  backgroundColor: PropTypes.string,
  onClick: PropTypes.func,
};
