import React from "react";
import styled from "styled-components";

export const Note = ({ onClick, note: { title, text, id, date, color } }) => {
  return (
    <NoteContainer onClick={onClick} color={color}>
      <NoteHeader>
        <Title>{title}</Title>
        <Date>{date}</Date>
      </NoteHeader>
      <Text>{text}</Text>
    </NoteContainer>
  );
};

const NoteContainer = styled.div`
  background-color: ${(p) => p.color};
  padding: 20px;
  margin: 0 10px 20px;
  border-radius: 10px;
  color: white;
  width: calc(100% / 3.2);
`;

const NoteHeader = styled.div`
  padding: 5px 0;
  border-bottom: 1px solid white;
`;

const Title = styled.div`
  font-size: 20px;
  font-weight: bold;
`;
const Date = styled.div`
  font-size: 12px;
  margin-bottom: 10px;
`;
const Text = styled.p`
  text-align: center;
  white-space: nowrap;
  text-overflow: ellipsis;
  overflow: hidden;
`;
