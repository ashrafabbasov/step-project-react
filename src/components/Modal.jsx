import React from 'react';


export const Modal = ({header, closeButton, text, close,actions} ) => {
    return(
        <div className="modal">
            <header className="header">
                {header}
                {closeButton && <button onClick = {close} className="close-btn"></button>}
            </header>
            <div className="modal-body">

            <p className="modal-text">{text}</p>
            <div className="actions">{actions}</div> 
            </div>
        </div>
    )
}


